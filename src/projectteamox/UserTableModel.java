/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectteamox;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;


/**
 *
 * @author Windows 10
 */
public class UserTableModel extends AbstractTableModel{
    String[] columnNames = {"FirstName","SurName","Username","Password","Phone","Weight","Height"};
    ArrayList<User> userList =Data.userlist;
    
    @Override
    public String getColumnName(int column) {
       return columnNames[column];
    }
    
    
    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex,int colIndex) {
              User user = userList.get(rowIndex);
       if(user == null) return "";
       switch (colIndex){
           case 0 : return user.getFname();
           case 1 : return user.getSname();
           case 2 : return user.getUsername();
           case 3 : return user.getPass();
           case 4 : return user.getPhone();
           case 5 : return user.getWeight();
           case 6 : return user.getHeight();

       }
       return "";
    }

    
}
