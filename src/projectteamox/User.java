/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectteamox;

import java.util.ArrayList;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author informatics
 */
public class User {
    String fname;
    String sname;
    String username;
    String pass;
    String phone;
    double weight;
    double height;

    public User(String fname, String sname, String username, String pass, String phone, double weight, double height) {
        this.fname = fname;
        this.sname = sname;
        this.username = username;
        this.pass = pass;
        this.phone = phone;
        this.weight = weight;
        this.height = height;
    }

    User() {
        
    }

    public String getFname() {
        return fname;
    }

    public String getSname() {
        return sname;
    }

    public String getUsername() {
        return username;
    }

    public String getPass() {
        return pass;
    }

    public String getPhone() {
        return phone;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "User{" + "fname=" + fname + ", sname=" + sname + ", username=" + username + ", pass=" + pass + ", phone=" + phone + ", weight=" + weight + ", height=" + height + '}';
    }

    void getFname(String fname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }





 


    

}
